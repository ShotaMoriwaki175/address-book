package study.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import study.bean.DataAccessBean;

@SuppressWarnings("serial")
@WebServlet(name="DeleteServlet", urlPatterns="/delete")
public class DeleteServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

		try {
        	//リクエストパラメータ値を取得
			String email = request.getParameter("email");

            //DataAccessBeanのインスタンスを作成
			DataAccessBean dab = new DataAccessBean();

            //DataAccessBeanのビジネスメソッド(deleteUserInfo()) にemailを渡して呼び出す
			dab.deleteUserInfo(email);

            // findall（一覧表示機能）にRedirect
			response.sendRedirect(request.getContextPath() + "/findall");

        }
        
        catch (SQLException e) {
            e.printStackTrace();
            // 例外発生時にエラーページ（/WEB-INF/error.jsp）へフォワード
            request.getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
        }

    }

}
