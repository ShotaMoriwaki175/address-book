package study.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(filterName="characterEncodingFilter",urlPatterns="/*",
			initParams={ @WebInitParam (name="CharacterEncoding", value="UTF-8" )})
public class CharacterEncodingFilter implements Filter {
    String charEnc = null;
    @Override
	public void destroy() {
    	
	}

    @Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(charEnc);
		chain.doFilter(request, response);
	}

    @Override
	public void init(FilterConfig Config) throws ServletException {
    	charEnc = Config.getInitParameter("CharacterEncoding");
	}

}
