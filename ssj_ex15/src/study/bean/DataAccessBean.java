package study.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import study.errors.DuplicateEmailException;


public class DataAccessBean {

	/** DataSource を保持する変数 */
	private static DataSource ds = null;

	/** lookup で使用するJNDI参照名 */
	private static final String JNDI_NAME = "java:comp/env/jdbc/ssjdb";

	/** DataSource 取得メソッド */
	private static DataSource getDataSource() throws NamingException {

		if(ds == null) {
			InitialContext ic = new InitialContext();
			ds = (DataSource) ic.lookup(JNDI_NAME);
		}
		return ds;
	}

	/**
	 * 全件検索処理
	 *
	 * @return UserInfoオブジェクトのコレクション
	 * @throws SQLException   DB接続関連の障害が発生した場合
	 */
	
	public Collection<UserInfo> findAllUserInfo() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

        try {
            //データ取得用SQL文
        	String sql = "SELECT name, yomi, zip, address, tel, email FROM addressbook";
        	
        	//戻り値用のCollectionオブジェクト
        	Collection<UserInfo> userList = new ArrayList<UserInfo>();

            //Connectionオブジェクトの取得
        	conn = getDataSource().getConnection();

        	//PreparedStatementオブジェクトの生成
        	ps = conn.prepareStatement(sql);

        	//SQL文の実行
        	rs = ps.executeQuery();

        	//検索結果の取得
            while (rs.next()) {
                //UserInfoオブジェクトの生成
            	UserInfo userInfo = new UserInfo();
            	//各カラムの値を取得し、UserInfoオブジェクトにセット
            	userInfo.setName(rs.getString("name"));
            	userInfo.setYomi(rs.getString("yomi"));
            	userInfo.setZip(rs.getString("zip"));
            	userInfo.setAddress(rs.getString("address"));
            	userInfo.setTel(rs.getString("tel"));
            	userInfo.setEmail(rs.getString("email"));
            	//テーブル一行分の結果を保持するUserInfoオブジェクトをCollectionに追加
            	userList.add(userInfo);
            }

            return userList;
        }
        //例外処理を実装
        
        catch (NamingException e) {
            e.printStackTrace();
            throw new SQLException(e);
        }
        
        finally {
        	if(rs != null){rs.close();}
        	if(ps != null){ps.close();}
        	if(conn != null){conn.close();}
        }
    }

	/**
	 * 新規登録処理
	 *
	 * @param userInfo 登録するユーザ情報
	 * @throws SQLException DB接続関連の障害が発生した場合
	 * @throws DuplicateEmailException 既に存在するメールアドレスを挿入しようとした場合
	 */
	public void registUserInfo(UserInfo userInfo) throws SQLException, DuplicateEmailException {
		Connection conn = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		try {
			//データ登録用SQL文
			String sql = "INSERT INTO addressbook(name, yomi, zip, address, tel, email)"
						+ "VALUES(?, ?, ?, ?, ?, ?)";
			//二重登録チェック用SQL文
			String sqlForCheck = "SELECT email FROM addressbook WHERE email=?";
			//Connectionオブジェクトの取得
			conn = getDataSource().getConnection();
			//二重登録チェック用のPreparedStatementオブジェクトの生成
			ps1 = conn.prepareStatement(sqlForCheck);
			//二重登録チェック用のパラメータ設定
			ps1.setString(1, userInfo.getEmail());
			//二重登録チェック用の検索
			rs = ps1.executeQuery();
			
			if(rs.next()){
				throw new DuplicateEmailException();
			}
			//データ登録用のPreparedStatementオブジェクトの生成
			ps2 = conn.prepareStatement(sql);

			//データ登録用のパラメータ設定
			ps2.setString(1, userInfo.getName());
			ps2.setString(2, userInfo.getYomi());
			ps2.setString(3, userInfo.getZip());
			ps2.setString(4, userInfo.getAddress());
			ps2.setString(5, userInfo.getTel());
			ps2.setString(6, userInfo.getEmail());

			//データ登録用のSQL文の実行
			ps2.executeUpdate();

		} catch (NamingException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			if(rs != null){rs.close();}
			if(ps1 != null){ps1.close();}
			if(ps2 != null){ ps2.close(); }
			if(conn != null) { conn.close(); }
		}
	}

	/**
	 * 削除処理
	 *
	 * @param email 削除する項目のメールアドレス
	 * @throws SQLException DB接続関連の障害が発生した場合
	 */
	public void deleteUserInfo(String email) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			//データ削除用SQL文
			String sql = "DELETE FROM addressbook WHERE email=?";
			//Connectionオブジェクトの取得
			conn = getDataSource().getConnection();
			//PreparedStatementオブジェクトの生成
			ps = conn.prepareStatement(sql);
			//パラメータの設定
			ps.setString(1, email);
			//SQL文の実行
			ps.executeUpdate();
			
		} catch (NamingException e) {
			e.printStackTrace();
			throw new SQLException(e);
		} finally {
			if(ps != null){ps.close();}
			if(conn != null){conn.close();}
		}

	}
}
